# purple-mm-sms

A libpurple plugin for sending and receiving SMS via Modemmanager


## Build and install

### Install dependencies

``` bash
sudo apt install libpurple-dev libmm-glib-dev modemmanager
```

### Build and install purple-mm-sms

``` bash
make
make install
```

## How to use it

Launch the "Manage Accounts" dialog in Pidgin. Choose 'ModemManager SMS' from the protocols list. Enter any string in the username entry and the modem-pin in the password-entry. The pin is optional, no need to enter it if the modem is already unlocked.

## Purple commands

- '/mm-sms help': Displays a list with available commands
- '/mm-sms info': Show modem info
- '/mm-sms rm_s [on; off]': Remove SMS from modem when sent
- '/mm-sms rm_r [on; off]': Remove SMS from modem when received
- '/mm-sms deli [on; off]': Request delivery reports from SMC
- '/mm-sms vali [2; 7; 30]': Set SMS validity period in days
